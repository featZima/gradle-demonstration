package demo.application.gradledemonstration;

import demo.application.gradledemonstration.domain.AddUseCase;
import demo.application.gradledemonstration.domain.UserNumber;

public final class Calculator {

    private Calculator() {
    }

    public static UserNumber calculate(UserNumber a, UserNumber b) {
        return AddUseCase.add(a, b);
    }
}
