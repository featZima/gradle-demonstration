package demo.application.gradledemonstration.domain;

public class AddUseCase {

    private AddUseCase() {
    }

    public static UserNumber add(UserNumber a, UserNumber b) {
        return new UserNumber(a.getValue() + b.getValue());
    }
}
