package demo.application.gradledemonstration.domain;

public class UserNumber {

    private double value;

    public UserNumber(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}
